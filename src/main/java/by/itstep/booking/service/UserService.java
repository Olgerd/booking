package by.itstep.booking.service;

import by.itstep.booking.dto.user.*;

import java.util.List;

public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById(int id);

    List<UserFullDto> findAll();

    void delete(int id);

    void changePassword(ChangeUserPasswordDto dto);

    UserFullDto changeRole(ChangeUserRoleDto dto);

    void block(Integer userId);
}
