package by.itstep.booking.service;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.dto.user.UserUpdateDto;

import java.util.List;

public interface HotelService {

    HotelFullDto create(HotelCreateDto dto);

    HotelFullDto update(HotelUpdateDto dto);

    HotelFullDto findById(int id);

    List<HotelFullDto> findAll();

    void delete(int id);
}
