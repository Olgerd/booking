package by.itstep.booking.service;

import by.itstep.booking.dto.apartment.ApartmentCreateDto;
import by.itstep.booking.dto.apartment.ApartmentFullDto;
import by.itstep.booking.dto.apartment.ApartmentUpdateDto;
import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.dto.user.UserUpdateDto;

import java.util.List;

public interface ApartmentService {

    ApartmentFullDto create(ApartmentCreateDto dto);

    ApartmentFullDto update(ApartmentUpdateDto dto);

    ApartmentFullDto findById(int id);

    List<ApartmentFullDto> findAll();

    void delete(int id);

}
