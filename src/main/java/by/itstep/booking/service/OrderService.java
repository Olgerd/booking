package by.itstep.booking.service;

import by.itstep.booking.dto.order.OrderCreateDto;
import by.itstep.booking.dto.order.OrderFullDto;
import by.itstep.booking.dto.order.OrderUpdateDto;
import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.dto.user.UserUpdateDto;

import java.util.List;

public interface OrderService {

    OrderFullDto create(OrderCreateDto dto);

    OrderFullDto update(OrderUpdateDto dto);

    OrderFullDto findById(int id);

    List<OrderFullDto> findAll();

    void delete(int id);

}
