package by.itstep.booking.service.impl;

import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValuesIsTakenException;
import by.itstep.booking.mapper.HotelMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class HotelServiceImpl implements HotelService {

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private HotelMapper hotelMapper;


    @Override
    @Transactional
    public HotelFullDto create(HotelCreateDto createDto){

        HotelEntity entityToSave = hotelMapper.map(createDto);

        HotelEntity entityWithSameName = hotelRepository.findByName(entityToSave.getName());

        if (entityWithSameName != null) {
            throw new UniqueValuesIsTakenException("Name is taken");
        }

        HotelEntity savedEntity = hotelRepository.save(entityToSave);

        HotelFullDto hotelDto = hotelMapper.map(savedEntity);

        System.out.println("HotelServiceImpl -> hotel was successfully created");
        return hotelDto;
    }

    @Override
    @Transactional
    public HotelFullDto update(HotelUpdateDto updateDto){

        HotelEntity hotelToUpdate = hotelRepository.getById(updateDto.getHotelId());
        if (hotelToUpdate == null) {
            throw new AppEntityNotFoundException("HotelEntity was not found by id: " + updateDto.getHotelId());
        }
        hotelToUpdate.setName(updateDto.getName());

        HotelEntity updatedHotel = hotelRepository.save(hotelToUpdate);
        HotelFullDto hotelDto = hotelMapper.map(updatedHotel);

        System.out.println("HotelServiceImpl -> Hotel was successfully updated" + hotelDto);

        return hotelDto;
    }

    @Override
    @Transactional
    public HotelFullDto findById(int id){

        HotelEntity foundHotel = hotelRepository.getById(id);

        if(foundHotel == null){
            throw  new AppEntityNotFoundException("HotelEntity was not found find");
        }
        HotelFullDto hotelDto = hotelMapper.map(foundHotel);

        System.out.println("HotelServiceImpl -> Hotel was successfully found" + hotelDto);
        return hotelDto;
    }

    @Override
    @Transactional
    public List<HotelFullDto> findAll(){

        List<HotelEntity> foundHotels = hotelRepository.findAll();

        List<HotelFullDto> dtos = hotelMapper.map(foundHotels);
        System.out.println("HotelServiceImpl -> Hotel was successfully found" + dtos);

        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id){

        HotelEntity entityToDelete = hotelRepository.getById(id);

        if( entityToDelete== null){
            throw  new AppEntityNotFoundException("HotelEntity was not found find");
        }
        entityToDelete.setDeletedAt(Instant.now());
        hotelRepository.deleteById(id);

        System.out.println("CategoryEntity deleted: " + entityToDelete);
    }
}
