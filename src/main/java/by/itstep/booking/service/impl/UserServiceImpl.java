package by.itstep.booking.service.impl;


import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.UserRole;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValuesIsTakenException;
import by.itstep.booking.exception.WrongUserPasswordException;
import by.itstep.booking.mapper.UserMapper;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {

        UserEntity entityToSave = userMapper.map(createDto);
        entityToSave.setBlocked(false);
        entityToSave.setRole(UserRole.USER);

        entityToSave.setPassword(passwordEncoder.encode(createDto.getPassword()  + createDto.getEmail()));

        UserEntity entityWithSameEmail = userRepository.findByEmail(entityToSave.getEmail());

        if(entityWithSameEmail != null){

            throw new UniqueValuesIsTakenException("Email is taken");
        }

        UserEntity savedEntity = userRepository.save(entityToSave);

        UserFullDto userDto = userMapper.map(savedEntity);

        System.out.println("UserServiceImpl -> user was successfully created");

        return userDto;
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {

        UserEntity userToUpdate = userRepository.getById(dto.getId());
        if(userToUpdate == null){
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getId());
        }
        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setFirstName(dto.getFirstName());
        userToUpdate.setLastName(dto.getLastName());
        userToUpdate.setPhone(dto.getPhone());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);

        System.out.println("UserServiceImpl -> User was successfully updated");
        return userDto;
    }

    @Override
    @Transactional
    public UserFullDto findById(int id) {

        UserEntity foundUser = userRepository.getById(id);

        if(foundUser == null){
            throw  new AppEntityNotFoundException("UserEntity was not found find");
        }
        UserFullDto userDto = userMapper.map(foundUser);

        System.out.println("UserServiceImpl -> User was successfully found");
        return userDto;
    }

    @Override
    @Transactional
    public List<UserFullDto> findAll() {

        List<UserEntity> foundUsers = userRepository.findAll();

        List<UserFullDto> dtos = userMapper.map(foundUsers);
        System.out.println("UserServiceImpl -> User was successfully found" + dtos);
        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id) {
        UserEntity foundUser = userRepository.getById(id);

        if(foundUser == null){
            throw  new AppEntityNotFoundException("UserEntity was not found find");
        }
        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);

        System.out.println("UserServiceImpl -> UserEntity id: " + id + " deleted!");

    }


    @Override
    @Transactional
    public void changePassword(ChangeUserPasswordDto dto) {

        UserEntity userToUpdate = userRepository.getById(dto.getUserId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId());
        }
        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        if (!userToUpdate.getPassword().equals(passwordEncoder.encode(dto.getOldPassword() + userToUpdate.getEmail()))) {
            throw new WrongUserPasswordException("Wrong password");
        }
        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword() + userToUpdate.getEmail()));
        userRepository.save(userToUpdate);

        System.out.println("UserServiceImpl -> User password was successfully updated");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(ChangeUserRoleDto dto) {
        UserEntity userToUpdate = userRepository.getById(dto.getUserId());
        if (userToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId());
        }

        userToUpdate.setRole(dto.getNewRole());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);
        System.out.println("UserServiceImpl -> User role was successfully updated");
        return userDto;
    }

    @Override
    @Transactional
    public void block(Integer userId) {
        UserEntity entityToUpdate = userRepository.getById(userId);
        if (entityToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + userId);
        }
        entityToUpdate.setBlocked(true);
        userRepository.save(entityToUpdate);
        System.out.println("UserServiceImpl -> User was successfully updated.");
    }

    private boolean currentUserIsAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            if (authority.getAuthority().equals("ROLE_" + UserRole.ADMIN.name())) {
                return true;
            }
        }
        return false;
    }
    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }
}
