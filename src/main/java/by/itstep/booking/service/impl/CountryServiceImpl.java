package by.itstep.booking.service.impl;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValuesIsTakenException;
import by.itstep.booking.mapper.CountryMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryMapper countryMapper;

    @Autowired
    private CountryRepository countryRepository;

    @Override
    @Transactional
    public CountryFullDto create(CountryCreateDto createDto) {

        CountryEntity entityToSave = countryMapper.map(createDto);

        CountryEntity entityWithSameName = countryRepository.findByName(entityToSave.getName());

        if (entityWithSameName != null) {
            throw new UniqueValuesIsTakenException("Name is taken");
        }

        CountryEntity savedEntity = countryRepository.save(entityToSave);

        CountryFullDto countryDto = countryMapper.map(savedEntity);

        System.out.println("CountryServiceImpl -> country was successfully created");
        return countryDto;
    }

    @Override
    @Transactional
    public CountryFullDto update(CountryUpdateDto updateDto) {

        CountryEntity countryToUpdate = countryRepository.getById(updateDto.getCountryId());
        if (countryToUpdate == null) {
            throw new AppEntityNotFoundException("CountryEntity was not found by id: " + updateDto.getCountryId());
        }
        countryToUpdate.setName(updateDto.getName());

        CountryEntity updatedCountry = countryRepository.save(countryToUpdate);
        CountryFullDto countryDto = countryMapper.map(updatedCountry);

        System.out.println("CountryServiceImpl -> Country was successfully updated" + countryDto);

        return countryDto;
    }

    @Override
    @Transactional
    public CountryFullDto findById(int id) {

        CountryEntity foundCountry = countryRepository.getById(id);

        if (foundCountry == null) {
            throw new AppEntityNotFoundException("CountryEntity was not found find");
        }
        CountryFullDto countryDto = countryMapper.map(foundCountry);

        System.out.println("CountryServiceImpl -> Country was successfully found" + countryDto);
        return countryDto;
    }

    @Override
    @Transactional
    public List<CountryFullDto> findAll(){

        List<CountryEntity> foundCountries = countryRepository.findAll();

        List<CountryFullDto> dtos = countryMapper.map(foundCountries);
        System.out.println("CategoryServiceImpl -> Category was successfully found" +dtos);
        return dtos;
    }


    @Override
    @Transactional
    public void delete(int id){

        CountryEntity entityToDelete = countryRepository.getById(id);

        if( entityToDelete== null){

            throw  new AppEntityNotFoundException("CountryEntity was not found find");
        }
        entityToDelete.setDeletedAt(Instant.now());
        countryRepository.deleteById(id);

        System.out.println("CountryEntity deleted: " + entityToDelete);

    }
}
