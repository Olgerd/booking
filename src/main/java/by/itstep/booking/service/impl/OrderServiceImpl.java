package by.itstep.booking.service.impl;

import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.order.OrderCreateDto;
import by.itstep.booking.dto.order.OrderFullDto;
import by.itstep.booking.dto.order.OrderUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.OrderEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValuesIsTakenException;
import by.itstep.booking.mapper.OrderMapper;
import by.itstep.booking.repository.OrderRepository;
import by.itstep.booking.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    @Transactional
    public OrderFullDto create(OrderCreateDto createDto){

        OrderEntity entityToSave = orderMapper.map(createDto);

        OrderEntity entityWithSameUserEmail = orderRepository.findByUserEmail(entityToSave.getUser().getEmail());

        if (entityWithSameUserEmail != null) {
            throw new UniqueValuesIsTakenException("User is taken");
        }

        OrderEntity savedEntity = orderRepository.save(entityToSave);

        OrderFullDto orderDto = orderMapper.map(savedEntity);

        System.out.println("OrderServiceImpl -> Order was successfully created");
        return orderDto;

    }

    @Override
    @Transactional
    public OrderFullDto update(OrderUpdateDto updateDto){

        OrderEntity orderToUpdate = orderRepository.getById(updateDto.getOrderId());
        if (orderToUpdate == null) {
            throw new AppEntityNotFoundException("OrderEntity was not found by id: " + updateDto.getOrderId());
        }
        orderToUpdate.setDataIn(updateDto.getDataIn());
        orderToUpdate.setDataOut(updateDto.getDataOut());
        orderToUpdate.setStatus(updateDto.getStatus());
        orderToUpdate.setCost(updateDto.getCost());

        OrderEntity updatedOrder = orderRepository.save(orderToUpdate);
        OrderFullDto orderDto = orderMapper.map(updatedOrder);

        System.out.println("OrderServiceImpl -> Order was successfully updated" + orderDto);

        return orderDto;


    }

    @Override
    @Transactional
    public OrderFullDto findById(int id){

        OrderEntity foundOrder = orderRepository.getById(id);

        if (foundOrder == null) {
            throw new AppEntityNotFoundException("OrderEntity was not found find");
        }
        OrderFullDto orderDto = orderMapper.map(foundOrder);

        System.out.println("OrderServiceImpl -> Order was successfully found" + orderDto);
        return orderDto;
    }

    @Override
    @Transactional
    public List<OrderFullDto> findAll(){

        List<OrderEntity> foundOrders = orderRepository.findAll();

        List<OrderFullDto> dtos = orderMapper.map(foundOrders);
        System.out.println("OrderServiceImpl -> Order was successfully found" +dtos);
        return dtos;
    }

    @Override
    @Transactional
    public void delete(int id){

        OrderEntity entityToDelete = orderRepository.getById(id);

        if( entityToDelete== null){

            throw  new AppEntityNotFoundException("OrderEntity was not found find");
        }
        entityToDelete.setDeletedAt(Instant.now());
        orderRepository.deleteById(id);

        System.out.println("OrderEntity deleted: " + entityToDelete);

    }
}
