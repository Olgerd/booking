package by.itstep.booking.service.impl;

import by.itstep.booking.dto.apartment.ApartmentCreateDto;
import by.itstep.booking.dto.apartment.ApartmentFullDto;
import by.itstep.booking.dto.apartment.ApartmentUpdateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.entity.ApartmentEntity;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValuesIsTakenException;
import by.itstep.booking.mapper.ApartmentMapper;
import by.itstep.booking.repository.ApartmentRepository;
import by.itstep.booking.service.ApartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class ApartmentServiceImpl implements ApartmentService {

    @Autowired
    private ApartmentRepository apartmentRepository;

    @Autowired
    private ApartmentMapper apartmentMapper;

    @Override
    @Transactional
    public ApartmentFullDto create(ApartmentCreateDto createDto){

        ApartmentEntity entityToSave = apartmentMapper.map(createDto);

        ApartmentEntity entityWithSameName = apartmentRepository.findByNumber(entityToSave.getNumber());

        if (entityWithSameName != null) {
            throw new UniqueValuesIsTakenException("Number is taken");
        }

        ApartmentEntity savedEntity = apartmentRepository.save(entityToSave);

        ApartmentFullDto apartmentDto = apartmentMapper.map(savedEntity);

        System.out.println("ApartmentServiceImpl -> Apartment was successfully created");
        return apartmentDto;

    }

    @Override
    @Transactional
    public ApartmentFullDto update(ApartmentUpdateDto updateDto){

        ApartmentEntity apartmentToUpdate = apartmentRepository.getById(updateDto.getApartmentId());
        if (apartmentToUpdate == null) {
            throw new AppEntityNotFoundException("ApartmentEntity was not found by id: " + updateDto.getApartmentId());
        }
        apartmentToUpdate.setNumber(updateDto.getNumber());
        apartmentToUpdate.setPlaces(updateDto.getPlaces());
        apartmentToUpdate.setCost(updateDto.getCost());
        apartmentToUpdate.setDescription(updateDto.getDescription());

        ApartmentEntity updatedApartment = apartmentRepository.save(apartmentToUpdate);
        ApartmentFullDto apartmentDto = apartmentMapper.map(updatedApartment);

        System.out.println("ApartmentServiceImpl -> Apartment was successfully updated" + apartmentDto);

        return apartmentDto;

    }

    @Override
    @Transactional
    public ApartmentFullDto findById(int id){

        ApartmentEntity foundApartment = apartmentRepository.getById(id);

        if (foundApartment == null) {
            throw new AppEntityNotFoundException("ApartmentEntity was not found find");
        }
        ApartmentFullDto apartmentDto = apartmentMapper.map(foundApartment);

        System.out.println("ApartmentServiceImpl -> Apartment was successfully found" + apartmentDto);
        return apartmentDto;

    }

    @Override
    @Transactional
    public List<ApartmentFullDto> findAll(){

        List<ApartmentEntity> foundApartments = apartmentRepository.findAll();

        List<ApartmentFullDto> dtos = apartmentMapper.map(foundApartments);
        System.out.println("ApartmentServiceImpl -> Apartment was successfully found" +dtos);
        return dtos;

    }

    @Override
    @Transactional
    public void delete(int id){

        ApartmentEntity entityToDelete = apartmentRepository.getById(id);

        if( entityToDelete== null){
            throw  new AppEntityNotFoundException("ApartmentEntity was not found find");
        }
        entityToDelete.setDeletedAt(Instant.now());
        apartmentRepository.deleteById(id);

        System.out.println("ApartmentEntity deleted: " + entityToDelete);

    }
}
