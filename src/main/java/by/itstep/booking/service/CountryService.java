package by.itstep.booking.service;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.dto.user.UserUpdateDto;

import java.util.List;

public interface CountryService {

    CountryFullDto create(CountryCreateDto dto);

    CountryFullDto update(CountryUpdateDto dto);

    CountryFullDto findById(int id);

    List<CountryFullDto> findAll();

    void delete(int id);
}
