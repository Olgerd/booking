package by.itstep.booking.controller;


import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CountryController {

    @Autowired
    private CountryService countryService;

    @PostMapping("/countries")
    public CountryFullDto create(@Valid @RequestBody CountryCreateDto dto){
        return countryService.create(dto);
    }

    @PutMapping("/countries")
    public CountryFullDto update(@Valid @RequestBody CountryUpdateDto updateDto){
        CountryFullDto updatedCountry = countryService.update(updateDto);
        return updatedCountry;
    }

    @GetMapping("/countries/country_id")
    public CountryFullDto findById(@PathVariable Integer id){
        return countryService.findById(id);
    }

    @GetMapping("/countries")
    public List<CountryFullDto> findAllCountries(){
        List<CountryFullDto> allCountries = countryService.findAll();
        return allCountries;
    }

    @DeleteMapping("/countries/country_id")
    public void delete(@PathVariable int id){
        countryService.delete(id);
    }

}
