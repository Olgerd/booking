package by.itstep.booking.controller;


import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class HotelController {

    @Autowired
    private HotelService hotelService;

    @PostMapping("/hotels")
    public HotelFullDto create(@Valid @RequestBody HotelCreateDto createDto){ return hotelService.create(createDto); }

    @PutMapping("/hotels")
    public HotelFullDto update(@Valid @RequestBody HotelUpdateDto updateDto){
        HotelFullDto updatedHotel = hotelService.update(updateDto);
        return updatedHotel;
    }

    @GetMapping("/hotels/hotel_id")
    public HotelFullDto findById(@PathVariable Integer id){ return hotelService.findById(id); }

    @GetMapping("/hotels")
    public List<HotelFullDto> findAllHotels(){
        List<HotelFullDto> allHotels = hotelService.findAll();
        return allHotels;
    }

    @DeleteMapping("/hotels/hotel_id")
    public void delete(@PathVariable int id){ hotelService.delete(id); }
}
