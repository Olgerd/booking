package by.itstep.booking.controller;
import by.itstep.booking.dto.user.*;

import by.itstep.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@RestController
public class UserController {



    @Autowired
    private UserService userService;

    @GetMapping("/users/{user_id}")
    public UserFullDto findById(@PathVariable Integer id){
        return  userService.findById(id);
    }

    @GetMapping("/users")
    public List<UserFullDto> findAllUsers(){
       List<UserFullDto> allUsers = userService.findAll();
        return allUsers;
    }

    @PutMapping("/users")
    public UserFullDto update(@Valid @RequestBody UserUpdateDto updateDto){
        UserFullDto updatedUser = userService.update(updateDto);
        return updatedUser;
    }

    @DeleteMapping("/users/user_id")
    public void delete(@PathVariable int id){
        userService.delete(id);
    }

    @PutMapping("/users/role")
    public void changeRole(@RequestBody ChangeUserRoleDto dto){
        userService.changeRole(dto);
    }

    @PostMapping ("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto dto){
        return userService.create(dto);
    }

    @PutMapping("/users/password")
    public void changePassword(@RequestBody ChangeUserPasswordDto dto){
        userService.changePassword(dto);
    }

    @PutMapping("/users/{user_id}/block")
    public void block(
            @PathVariable Integer id,
            @RequestHeader("Authorization") String authorization
    ) throws Exception{
        userService.block(id);
    }
}
