package by.itstep.booking.controller;


import by.itstep.booking.dto.order.OrderCreateDto;
import by.itstep.booking.dto.order.OrderFullDto;
import by.itstep.booking.dto.order.OrderUpdateDto;
import by.itstep.booking.service.OrderService;
import org.apache.tomcat.jni.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/orders")
    public OrderFullDto create(@Valid @RequestBody OrderCreateDto createDto){ return orderService.create(createDto);}

    @PutMapping("/orders")
    public OrderFullDto update(@Valid @RequestBody OrderUpdateDto updateDto){
        OrderFullDto updatedOrder = orderService.update(updateDto);
        return updatedOrder;
    }

    @GetMapping("/orders/order_id")
    public OrderFullDto findById(@PathVariable Integer id){return orderService.findById(id);}

    @GetMapping("/orders")
    public List<OrderFullDto> findAllOrders(){
        List<OrderFullDto> allOrders = orderService.findAll();
        return allOrders;
    }

    @DeleteMapping("/orders/order_id")
    public void delete(@PathVariable int id){orderService.delete(id);}

}
