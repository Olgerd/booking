package by.itstep.booking.controller;

import by.itstep.booking.dto.apartment.ApartmentCreateDto;
import by.itstep.booking.dto.apartment.ApartmentFullDto;
import by.itstep.booking.dto.apartment.ApartmentUpdateDto;
import by.itstep.booking.service.ApartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ApartmentController {

    @Autowired
    private ApartmentService apartmentService;

    @PostMapping("/apartments")
    public ApartmentFullDto create(@Valid @RequestBody ApartmentCreateDto createDto){
        return apartmentService.create(createDto);
    }

    @PutMapping("/apartments")
    public ApartmentFullDto update(@Valid @RequestBody ApartmentUpdateDto updateDto){
        ApartmentFullDto updatedApartment = apartmentService.update(updateDto);
        return updatedApartment;
    }

    @GetMapping("/apartments/apartment_id")
    public ApartmentFullDto findById(@PathVariable Integer id){ return apartmentService.findById(id); }

    @GetMapping("/apartments")
    public List<ApartmentFullDto> findAllApartments (){
        List<ApartmentFullDto> allApartments = apartmentService.findAll();
        return allApartments;
    }

    @DeleteMapping("/apartments/apartments_id")
    public void delete(@PathVariable int id){ apartmentService.delete(id); }

}
