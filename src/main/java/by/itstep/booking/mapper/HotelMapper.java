package by.itstep.booking.mapper;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.entity.HotelEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HotelMapper {

    HotelFullDto map(HotelEntity entity);

    List<HotelFullDto> map(List<HotelEntity> entities);

    HotelEntity map(HotelCreateDto dto);
}
