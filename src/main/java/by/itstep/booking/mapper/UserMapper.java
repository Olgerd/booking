package by.itstep.booking.mapper;

import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(componentModel = "spring")
public interface UserMapper {

    UserFullDto map(UserEntity entity);

    List<UserFullDto> map(List<UserEntity> entities);

    UserEntity map(UserCreateDto dto);

}
