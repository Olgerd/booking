package by.itstep.booking.mapper;

import by.itstep.booking.dto.order.OrderCreateDto;
import by.itstep.booking.dto.order.OrderFullDto;
import by.itstep.booking.entity.OrderEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderFullDto map(OrderEntity entity);

    List<OrderFullDto> map(List<OrderEntity> entities);

    OrderEntity map(OrderCreateDto dto);
}
