package by.itstep.booking.mapper;

import by.itstep.booking.dto.apartment.ApartmentCreateDto;
import by.itstep.booking.dto.apartment.ApartmentFullDto;
import by.itstep.booking.entity.ApartmentEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ApartmentMapper {

    ApartmentFullDto map(ApartmentEntity entity);

    List<ApartmentFullDto> map(List<ApartmentEntity> entities);

    ApartmentEntity map(ApartmentCreateDto dto);
}
