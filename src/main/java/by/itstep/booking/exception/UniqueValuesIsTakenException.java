package by.itstep.booking.exception;

public class UniqueValuesIsTakenException extends RuntimeException{

    public UniqueValuesIsTakenException(String message){

        super(message);

    }
}
