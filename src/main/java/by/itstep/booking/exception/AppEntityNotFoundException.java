package by.itstep.booking.exception;

public class AppEntityNotFoundException extends RuntimeException{

    public AppEntityNotFoundException(String message){
        super(message);
    }
}
