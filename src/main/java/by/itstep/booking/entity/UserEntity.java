package by.itstep.booking.entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "users")
@Where(clause = "deleted_at IS NULL")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "password")
    private String  password;

    @Column(name = "purse")
    private Double purse;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserRole role;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", fetch  = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<OrderEntity> orders = new ArrayList<>();

    @Column(name="blocked")
    private Boolean blocked;

    @Column(name = "deleted_at")
    private Instant deletedAt;
}
