package by.itstep.booking.entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "orders")
@Where(clause = "deleted_at IS NULL")
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "data_in")
    private Date dataIn;

    @Column(name = "data_out")
    private Date dataOut;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    private OrderStatus status;

    @Column(name = "cost")
    private double cost;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "order", fetch  = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ApartmentEntity> apartments = new ArrayList<>();

    @Column(name = "deleted_at")
    private Instant deletedAt;
}
