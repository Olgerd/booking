package by.itstep.booking.entity;

public enum UserRole {
    USER, ADMIN
}
