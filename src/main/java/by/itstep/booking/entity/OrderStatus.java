package by.itstep.booking.entity;

public enum OrderStatus {
    REQUESTED,IN_DISCUSSION,APPROVED,PAID,REJECTED
}
