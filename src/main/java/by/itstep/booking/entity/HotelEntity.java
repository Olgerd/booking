package by.itstep.booking.entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "hotels")
@Where(clause = "deleted_at IS NULL")
public class HotelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "hotel_id")
    private Integer hotelId;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "stars")
    private int stars;

    @Column(name = "rating")
    private int rating;

    @Column(name = "cost")
    private double cost;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryEntity country;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "hotel", fetch  = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ApartmentEntity> apartments = new ArrayList<>();

    @Column(name = "deleted_at")
    private Instant deletedAt;
}
