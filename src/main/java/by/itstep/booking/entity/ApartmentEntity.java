package by.itstep.booking.entity;
import lombok.Data;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "apartments")
@Where(clause = "deleted_at IS NULL")
public class ApartmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "apartment_id")
    private Integer apartmentId;

    @Column(name = "number")
    private String number;

    @Column(name = "places")
    private int places;

    @Column(name = "cost")
    private double cost;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private HotelEntity hotel;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private OrderEntity order;

    @Column(name = "deleted_at")
    private Instant deletedAt;
}
