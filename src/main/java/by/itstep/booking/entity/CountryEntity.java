package by.itstep.booking.entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "countries")
@Where(clause = "deleted_at IS NULL")
public class CountryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private Integer countryId;

    @Column(name = "name")
    private String name;

    @Column(name="blocked")
    private Boolean blocked;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "country", fetch  = FetchType.LAZY,cascade = CascadeType.ALL)
    private  List<HotelEntity> hotels= new ArrayList<>();

    @Column(name = "deleted_at")
    private Instant deletedAt;
}
