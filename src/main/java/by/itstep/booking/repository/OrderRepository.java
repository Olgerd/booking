package by.itstep.booking.repository;

import by.itstep.booking.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity,Integer> {

    OrderEntity findByUserEmail(String email);
}
