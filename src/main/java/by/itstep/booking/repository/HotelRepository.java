package by.itstep.booking.repository;

import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotelRepository extends JpaRepository<HotelEntity,Integer> {

    HotelEntity findByName(String name);
}
