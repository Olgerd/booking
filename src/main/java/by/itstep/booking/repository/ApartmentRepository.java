package by.itstep.booking.repository;

import by.itstep.booking.entity.ApartmentEntity;
import by.itstep.booking.entity.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApartmentRepository extends JpaRepository<ApartmentEntity,Integer> {

    ApartmentEntity findByNumber(String number);
}
