package by.itstep.booking.repository;

import by.itstep.booking.entity.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<CountryEntity,Integer> {

    CountryEntity findByName(String name);
}
