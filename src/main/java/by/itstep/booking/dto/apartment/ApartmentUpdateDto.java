package by.itstep.booking.dto.apartment;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ApartmentUpdateDto {

    @NotNull
    Integer apartmentId;

    @NotNull
    String number;

    @NotNull
    int places;

    @NotNull
    double cost;

    @NotBlank
    String description;
}
