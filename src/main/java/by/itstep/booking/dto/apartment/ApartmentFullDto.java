package by.itstep.booking.dto.apartment;

import lombok.Data;

@Data
public class ApartmentFullDto {

    Integer apartmentId;
    String number;
    int places;
    double cost;
    String description;
}
