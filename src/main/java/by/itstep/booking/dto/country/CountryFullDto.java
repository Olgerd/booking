package by.itstep.booking.dto.country;


import by.itstep.booking.entity.HotelEntity;
import lombok.Data;

import java.util.List;

@Data
public class CountryFullDto {

    Integer countryId;
    String name;
    List<HotelEntity> hotels;

}
