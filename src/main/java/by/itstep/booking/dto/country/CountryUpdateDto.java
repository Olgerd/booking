package by.itstep.booking.dto.country;

import by.itstep.booking.entity.HotelEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CountryUpdateDto {

    @NotNull
    Integer countryId;

    @NotBlank
    String name;

    List<HotelEntity> hotels;
}
