package by.itstep.booking.dto.country;

import by.itstep.booking.entity.HotelEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class CountryCreateDto {

    @NotBlank
    String name;

    List<HotelEntity> hotels;
}
