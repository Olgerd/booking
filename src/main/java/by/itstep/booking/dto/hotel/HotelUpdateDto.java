package by.itstep.booking.dto.hotel;

import by.itstep.booking.entity.ApartmentEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class HotelUpdateDto {

    @NotNull
    Integer hotelId;

    @NotBlank
    String name;

    @NotBlank
    String address;

    int stars;

    int rating;

    @NotNull
    double cost;

    @NotEmpty
    List<ApartmentEntity> apartments;
}
