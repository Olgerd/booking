package by.itstep.booking.dto.hotel;

import by.itstep.booking.entity.ApartmentEntity;
import lombok.Data;

import java.util.List;

@Data
public class HotelFullDto {

    Integer hotelId;
    String name;
    String address;
    int stars;
    int rating;
    double cost;
    List<ApartmentEntity> apartments;

}
