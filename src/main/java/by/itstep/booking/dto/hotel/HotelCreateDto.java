package by.itstep.booking.dto.hotel;

import by.itstep.booking.entity.ApartmentEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class HotelCreateDto {

    @NotBlank
    String name;

    @NotBlank
    String address;

    @NotNull
    int stars;

    @NotNull
    int rating;

    @NotNull
    double cost;

    List<ApartmentEntity> apartments;
}
