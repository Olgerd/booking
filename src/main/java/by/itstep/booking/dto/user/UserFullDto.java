package by.itstep.booking.dto.user;


import by.itstep.booking.entity.UserRole;
import lombok.Data;


@Data
public class UserFullDto {


    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private UserRole role;

    private String phone;


}
