package by.itstep.booking.dto.order;

import by.itstep.booking.entity.ApartmentEntity;
import by.itstep.booking.entity.OrderStatus;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class OrderFullDto {

    Integer orderId;
    Date dataIn;
    Date dataOut;
    OrderStatus status;
    double cost;
    List<ApartmentEntity> apartments;
}
