package by.itstep.booking.dto.order;

import by.itstep.booking.entity.ApartmentEntity;
import by.itstep.booking.entity.OrderStatus;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class OrderCreateDto {

    @NotNull
    Date dataIn;

    @NotNull
    Date dataOut;

    @NotBlank
    OrderStatus status;

    @NotNull
    double cost;

    List<ApartmentEntity> apartments;
}
