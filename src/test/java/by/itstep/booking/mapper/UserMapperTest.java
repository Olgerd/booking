package by.itstep.booking.mapper;

import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void mapToDto_happyPath(){
        //given
        UserEntity entity = new UserEntity();
        entity.setUserId(15);
        entity.setEmail("bob@gmail.com");
        entity.setRole(UserRole.ADMIN);
        entity.setFirstName("Bob");
        entity.setLastName("Bobson");
        entity.setPassword("12345678");
        entity.setPhone("+375 29 353535454");
        entity.setPurse(100.5);
        // when
        UserFullDto dto = userMapper.map(entity);

        //then
        Assertions.assertNotNull(dto);
        System.out.println("-->" + dto);
    }
}
